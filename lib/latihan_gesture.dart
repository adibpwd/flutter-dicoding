import 'package:flutter/material.dart';

class LatihanGestures extends StatefulWidget {
  @override
  _LatihanGesturesState createState() => _LatihanGesturesState();
}

class _LatihanGesturesState extends State<LatihanGestures> {
  final double boxSize = 300.0;
  int numTaps = 0;
  int numDoubleTaps = 0;
  int numLongPress = 0;
  String imgGif = 'img/spongebob1.gif';

  double posX = 0.0;
  double posY = 0.0;
  @override
  Widget build(BuildContext context) {
    if (posX == 0) {
      center(context);
    }
    return Scaffold(
      appBar: AppBar(
        title: Text('Gesture Detector'),
      ),
      body: Stack(
        children: [
          Positioned(
            top: posY,
            left: posX,
            child: GestureDetector(
              onTap: () {
                setState(() {
                  numTaps++;
                });
              },
              onDoubleTap: () {
                setState(() {
                  numDoubleTaps++;
                });
              },
              onLongPress: () {
                setState(() {
                  numLongPress++;
                });
              },
              onPanUpdate: (DragUpdateDetails details) {
                setState(() {
                  double deltaX = details.delta.dx;
                  double deltaY = details.delta.dy;
                  posX += deltaX;
                  posY += deltaY;
                  imgGif = 'img/spongebob2.gif';
                });
              },
              onPanEnd: (e) {
                setState(() {
                  imgGif = 'img/spongebob1.gif';
                });
              },
              child: Container(
                width: boxSize,
                height: boxSize,
                child: Image.asset(
                  imgGif,
                ),
                // decoration: BoxDecoration(color: Colors.red),
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        color: Colors.yellow,
        padding: const EdgeInsets.all(16.0),
        child: Text(
          'Taps: $numTaps - Double Taps: $numDoubleTaps - Long Press: $numLongPress',
          style: TextStyle(fontSize: 15, fontFamily: 'roboto'),
        ),
      ),
    );
  }

  void center(BuildContext context) {
    posX = (MediaQuery.of(context).size.width / 2) - boxSize / 2;
    posY = (MediaQuery.of(context).size.height / 2) - boxSize / 2 - 30;

    setState(() {
      this.posX = posX;
      this.posY = posY;
    });
  }
}
