import 'package:flutter/material.dart';

class Page6 extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        padding: EdgeInsets.all(10),
        child: RaisedButton(
          child: Text('ke page7'),
          onPressed: () async {
            final result = await Navigator.pushNamed(context, '/page7');
            SnackBar snackbar = SnackBar(content: Text('$result'));
            _scaffoldKey.currentState.showSnackBar(snackbar);
          },
        ),
      ),
    );
  }
}
